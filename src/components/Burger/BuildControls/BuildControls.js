import React from 'react';
import './BuildControls.css';
import BuildControl from "./BuildControl/BuildControl";

const BuildControls = props => {
    const types = ['bacon', 'salad', 'cheese' ,'meat'];

   return (
       <div className="BuildControls">
           <p>Current Price: <strong>{props.price} KGS</strong></p>
           {types.map(type => {
               console.log('type', type);
               return <BuildControl
                   type={type}
                   added={() => props.ingredientAdded(type)}
                   removed={() => props.ingredientRemoved(type)}
                   disabled={props.disabled[type]}

               />;
           })}
           <button className="OrderButton"
                   disabled={!props.purchasable}
                   onClick={props.ordered}
           >ORDER NOW</button>


       </div>

   )

};

export default BuildControls;